﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;

namespace Xuno.Data {
   public static class Queries {
      public static Func<string, string> GetSettings(string connectionString) {
         string query = "SELECT * FROM `smtbl_settings`";

         var connection = new MySqlConnection(connectionString);
         connection.Open();
         var cmd = new MySqlCommand(query, connection);
         var dataReader = cmd.ExecuteReader();

         var settings = new Dictionary<string, string>();

         while (dataReader.Read()) {
            var key = dataReader.GetString("setting");
            var value = dataReader["value"].ToString();
            settings.Add(key, value);
         }

         dataReader.Close();
         connection.Close();

         return setting => settings.TryGetValue(setting, out var settingValue) ? settingValue : null;
      }

      public static Func<string, IEnumerable<(int id, string username, string email)>> GetUsersForUsernameFunc(string connectionString) {
         string query = @"
SELECT u.`id`, u.username, u.email
FROM `smtbl_user` as u
INNER JOIN `smtbl_usergroup` as ug ON ug.`id` = u.`usergroupid`
WHERE ug.`ldap_basedn` IS NULL AND(u.`email` = @username OR u.`username` = @username)";

         var connection = new MySqlConnection(connectionString);
         return username => {
            connection.Open();
            var cmd = new MySqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@username", username);
            var dataReader = cmd.ExecuteReader();
            var users = new List<(int id, string username, string email)>();
            while (dataReader.Read()) {
               var id = dataReader.GetInt32("id");
               var un = dataReader.GetString("username");
               var em = dataReader["email"].ToString();
               users.Add((id, un, em));
            }

            dataReader.Close();
            connection.Close();
            return users;
         };
      }

      public static Func<int, User> GetUserForIdFunc(string connectionString) {
         string query = @"
         SELECT m.*, ug.`name` AS `groupname`, MAX(aul.`datetime`) AS `lastlogindatetime`
         FROM `smtbl_user` as m
         JOIN `smtbl_usergroup` AS ug ON m.`usergroupid` = ug.`id`
         LEFT JOIN `smtbl_audituserlogin` AS aul ON aul.userid = m.id
         WHERE m.`id` = @userId
         GROUP BY m.`id` 
         ORDER BY `name`
         ";

         var connection = new MySqlConnection(connectionString);
         return userId => {
            connection.Open();
            var cmd = new MySqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@userId", userId);
            var dataReader = cmd.ExecuteReader();
            var users = new List<User>();
            while (dataReader.Read()) {
               var id = dataReader.GetInt32("id");
               var username = dataReader.GetString("username");
               var hash = dataReader.GetString("hash");
               var email = dataReader["email"].ToString();
               var password = dataReader.GetString("password");
               var userGroupId = dataReader.GetInt32("usergroupid");
               var disabled = dataReader.GetInt32("disabled");
               var type = dataReader.GetInt32("type");
               var apikey = dataReader.GetString("api_key");
               users.Add(new User(id, type, username, hash, password, email, userGroupId, apikey, disabled == 0));
            }

            dataReader.Close();
            connection.Close();

            return users.FirstOrDefault();
         };
      }
   }
   public static class MySqlFuncs {
      public static string GetMySqlConnectionString(string host, int port, string database, string username, string password) {
         // Connection String.
         var connString = "Server=" + host + ";Database=" + database+ ";port=" + port + ";User Id=" + username + ";password=" + password + ";SslMode=none";

         return connString;
      }

      public static string GetMySqlConnectionString(string host, string port, string database, string username, string password) {
         // Connection String.
         var connString = "Server=" + host + ";Database=" + database + ";port=" + port + ";User Id=" + username + ";password=" + password + ";SslMode=none";

         return connString;
      }
   }

   public class User {
      public User(int id, int type, string userName, string hash, string hashedPassword, string email, int userGroupId, string apiKey, bool isEnabled) {
         Id = id;
         Type = type;
         UserName = userName;
         Hash = hash;
         HashedPassword = hashedPassword;
         Email = email;
         UserGroupId = userGroupId;
         ApiKey = apiKey;
         IsEnabled = isEnabled;
      }
      public int Id { get; }
      public int Type { get; }
      public string UserName { get; }
      public string Hash { get; }
      public string HashedPassword { get; }
      public string Email { get; }
      public int UserGroupId { get; }
      public string ApiKey { get; }
      public bool IsEnabled { get; }

   }

   public class Person {

   }
}
