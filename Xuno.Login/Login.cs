﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Xuno.Data;
using XunoSharp.Transport;

namespace Xuno.Login {
   public class Login {
      public static ApiResponse GetLoginPage(Func<string, string> applicationSettings, ApiRequest request) {
         var cs = MySqlFuncs.GetMySqlConnectionString(applicationSettings("MySqlHost"), applicationSettings("MySqlPort"), applicationSettings("MySqlDatabase"), applicationSettings("MySqlUsername"), applicationSettings("MySqlPassword"));
         var schoolName = Queries.GetSettings(cs)("_schoolname");

         var x = FileReading.ReadFile("Login.html");
         var y = x.Replace("{SchoolName}", schoolName).Replace("{SessionGuid}", Guid.NewGuid().ToString());
         return ApiResponse.OK(y);
      }

      public static ApiResponse PerformLogin(Func<string, string> applicationSettings, ApiRequest request) {
         var username = request.GetFormValue("username");
         var password = request.GetFormValue("password");

         var cs = MySqlFuncs.GetMySqlConnectionString(applicationSettings("MySqlHost"), applicationSettings("MySqlPort"), applicationSettings("MySqlDatabase"), applicationSettings("MySqlUsername"), applicationSettings("MySqlPassword"));
         var users = Queries.GetUsersForUsernameFunc(cs)(username);
         if (users.Count() != 1) {
            return ApiResponse.WithStatusCode(500, "");
         }

         var user = users.FirstOrDefault();
         var fullUser = Queries.GetUserForIdFunc(cs)(user.id);
         if (string.IsNullOrWhiteSpace(fullUser.HashedPassword)) {
            return ApiResponse.WithStatusCode(500, "");
         }

         if (!BCrypt.Net.BCrypt.Verify(password, fullUser.HashedPassword)) {
            return ApiResponse.WithStatusCode(500, "");
         }

         if (!fullUser.IsEnabled) {
            return ApiResponse.WithStatusCode(500, "");
         }
         //set session stuff

         var x = FileReading.ReadFile("Login.html");
         var y = x.Replace("{SchoolName}", "xx").Replace("{SessionGuid}", Guid.NewGuid().ToString());
         return ApiResponse.OK(y);
      }
   }

   public static class FileReading {
      public static string ReadFile(string fileName) {
         string result;
         var assembly = Assembly.GetExecutingAssembly();
         using (var stream = assembly.GetManifestResourceStream($"Xuno.Login.{fileName}"))
         using (var reader = new StreamReader(stream)) {
            result = reader.ReadToEnd();
         }

         return result;
      }
   }
}