﻿using System.Text;
using Newtonsoft.Json.Linq;

namespace XunoSharp.Transport {
   public class ApiResponse {
      public static ApiResponse WithStatusCode(int statuCode, string body) {
         return new ApiResponse(statuCode, body);
      }

      public static ApiResponse Error(string error) {
         var obj = new {
            error = error
         };
         var response = JObject.FromObject(obj);
         return new ApiResponse(400, response.ToString());
      }

      public static ApiResponse OK(string body) {
         return new ApiResponse(200, body);
      }

      public static ApiResponse OK(byte[] body) {
         return new ApiResponse(200, body);
      }

      private ApiResponse(int statusCode, string body) {
         StatusCode = statusCode;
         Body = Encoding.UTF8.GetBytes(body);
      }

      private ApiResponse(int statusCode, byte[] body) {
         StatusCode = statusCode;
         Body = body;
      }

      public int StatusCode { get; }
      public byte[] Body { get; }
   }
}