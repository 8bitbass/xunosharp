﻿using System;
using System.Linq;

namespace XunoSharp.Transport {
   public class ApiRequest {
      public ApiRequest(string method, bool isHttps, string body, string queryString, string path) {
         Method = method;
         IsHttps = isHttps;
         Body = body;
         QueryString = queryString;
         Path = path;
      }

      public string Method { get; }

      public bool IsHttps { get; }

      public string Body { get; }

      public string QueryString { get; }

      public string Path { get; }
   }

   public static class ApiRequestExtensions {
      public static string GetQueryValue(this ApiRequest r, string key) {
         var safeQueryString = r.QueryString.StartsWith("?") ? r.QueryString.Substring(1) : r.QueryString;
         if (string.IsNullOrWhiteSpace(safeQueryString)) {
            return null;
         }
         return safeQueryString.Split('&').Select(item => new { Key = Uri.EscapeDataString(item.Substring(0, item.IndexOf('='))), Value = Uri.EscapeDataString(item.Substring(item.IndexOf('=') + 1)) }).FirstOrDefault(item => item.Key == key)?.Value ?? null; // Do the option thing
      }

      public static string GetFormValue(this ApiRequest r, string key) {
         var safeQueryString = r.Body.StartsWith("?") ? r.Body.Substring(1) : r.Body;
         if (string.IsNullOrWhiteSpace(safeQueryString)) {
            return null;
         }
         return safeQueryString.Split('&').Select(item => new { Key = Uri.EscapeDataString(item.Substring(0, item.IndexOf('='))), Value = Uri.EscapeDataString(item.Substring(item.IndexOf('=') + 1)) }).FirstOrDefault(item => item.Key == key)?.Value ?? null; // Do the option thing
      }
   }
}