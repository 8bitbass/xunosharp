﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using XunoSharp.Transport;

namespace XunoSharp {
   public class Startup {
      public Startup(IHostingEnvironment env) {
         var builder = new ConfigurationBuilder().SetBasePath(env.ContentRootPath);
         if (env.IsDevelopment()) {
            builder.AddUserSecrets<Startup>();
         }
         else if (env.IsProduction()) {
            builder.AddEnvironmentVariables();
         }
         Configuration = builder.Build();
      }

      public IConfigurationRoot Configuration { get; }

      // This method gets called by the runtime. Use this method to add services to the container.
      // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
      public void ConfigureServices(IServiceCollection services) {
      }

      // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
      public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
         if (env.IsDevelopment()) {
            app.UseDeveloperExceptionPage();
         }

         app.Run(async (context) => {
            var request = context.Request.ConvertToApiRequest();
            var handler = Router.GetRouteHandler(request);
            var response = handler(GetLookup(Configuration), request);
            context.Response.ContentLength = response.Body.Length;
            context.Response.StatusCode = response.StatusCode;
            await context.Response.Body.WriteAsync(response.Body, 0, response.Body.Length);
         });
      }

      public static Func<string, string> GetLookup(IConfigurationRoot configuration) {
         var lookup = new Dictionary<string, string>();
         return key => {
            if (lookup.TryGetValue(key, out var value)) {
               return value;
            }
            var configValue = configuration[key];
            lookup.Add(key, configValue);
            return configValue;
         };
      }
   }
   internal static class Ext {
      public static ApiRequest ConvertToApiRequest(this HttpRequest r) {
         return new ApiRequest(r.Method, r.IsHttps, r.GetBody().Result, r.QueryString.Value, r.Path);
      }

      public static async Task<string> GetBody(this HttpRequest request) {
         var requestBody = request.Body;

         string responseBody;
         using (var reader = new StreamReader(requestBody)) {
            responseBody = await reader.ReadToEndAsync();
         }
         return responseBody;
      }
   }
}
