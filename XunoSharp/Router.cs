﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Xuno.Login;
using XunoSharp.Transport;

namespace XunoSharp {
   public static class Router {
      public static Func<Func<string, string>, ApiRequest, ApiResponse> GetRouteHandler(ApiRequest request) {
         var path = request.Path.ToString().ToLower();
         if (!path.EndsWith("/")) {
            path += "/";
         }

         if (request.Method.ToUpper() == "POST") {
            var rDo = request.GetFormValue("do");
            switch (rDo) {
               case "login": return Login.PerformLogin;

            }
         }

         switch (path) {
            case "/": return Login.GetLoginPage;

         }
         return NotImplemented;
      }

      public static ApiResponse NotImplemented(Func<string, string> applicationSettings, ApiRequest request) => ApiResponse.WithStatusCode(StatusCodes.Status501NotImplemented, "");
      public static ApiResponse InvalidMethodType(Func<string, string> applicationSettings, ApiRequest request) => ApiResponse.WithStatusCode(StatusCodes.Status405MethodNotAllowed, "incorrect method");
   }
}